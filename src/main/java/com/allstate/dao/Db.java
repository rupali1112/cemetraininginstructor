package com.allstate.dao;

import java.util.List;

import com.allstate.entities.Employee;

public interface Db {
    int count();
    Employee find(int id);
    List<Employee> findall();
}
